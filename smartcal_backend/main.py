# from search_problem import search_helper
from src.search import ScheduleSearchProblem
from src.calender_types import ProblemName, EventsList, MyAndroidSideCalendar
from src.encoding import encode, decode
from src.calender_types import EventData
import zlib
import sys
import time
import datetime
import traceback
from pprint import pprint
import requests
import json
import base64

from src.config import DEBUG, LOG_INFO, LOG_DEBUG



def log(tag="TAG", message="NO MESSAGE", log_type="info"):
    if log_type == "info":
        if LOG_INFO:
            print("{}: {}".format(tag, message))
    if log_type == "debug":
        if LOG_DEBUG:
            print("{}: {}".format(tag, message))

def get_tasks(raw_tasks=None):
    print("Parsing the following tasks of type {} .".format(type(raw_tasks)))
    print(raw_tasks)
    if type(raw_tasks) == str:
        print("Evaluating raw tasks")
        raw_tasks = eval(raw_tasks)
        print(raw_tasks)

    data = []
    if raw_tasks:
        for task in raw_tasks:
            try:
                print("\tNew Task: {}".format(task))
                data.append(EventData(task["name"], task["duration"]))
                print("\tSuccessfully added!")

            except KeyError as ke:
                print(ke)

    elif DEBUG:
        data = [
            EventData("חדר כושר", datetime.timedelta(hours=float(2))),
            EventData("מפגש חטיבה ביזמות", datetime.timedelta(hours=float(1)))
        ]

    else:
        raise NotImplemented
        # while True:
        #     name = input("task name:")
        #     if name == "q":
        #         break
        #     duration = input("duration:")
        #
        #     duration = datetime.timedelta(hours=float(duration))
        #     data.append(EventData(name, duration))
    return data


def parce_args(arg):
    if arg == "today":
        return ProblemName.TODAY
    if arg == "tomorrow":
        return ProblemName.TOMORROW
    if arg == "week":
        return ProblemName.THIS_WEEK
    if arg == "next_week":
        return ProblemName.NEXT_WEEK


def print_base_info(state: EventsList, start: datetime.datetime, end: datetime.datetime):
    log("base_info", "start time: " + start.isoformat())
    log("base_info", "end time: " + end.isoformat())
    log("base_info", "*****")
    [log("base_info", str(event)) for event in state.get_events()]
    log("base_info", "*****")


def get_problem(problem_name: ProblemName, input_data, data=None, print_base=False):
    cal = MyAndroidSideCalendar(data)
    base_day= cal.get_events(problem_name)
    for event in base_day:
        log("Base Day Event", repr(event), log_type="debug")
    _problem = ScheduleSearchProblem(base_day, cal, base_day.get_start(), base_day.get_end(), input_data)
    print_base_info(base_day, base_day.get_start(), base_day.get_end())
    return _problem, base_day, cal, time


def insert_solution(solution, cal):
    for event in solution:
        if event.moveable:
            cal.insert_event(event)


def update(best_solution, calendar):
    insert_solution(best_solution, calendar)


def _validate_input(input):
    assert "tasks" in input, "Request data must include 'tasks' field. For example {'tasks': ['name': task1, 'duration': 2], ...}"
    assert "events" in input, "Request data must include 'data' field. For example {'tasks': [...], 'events': 'some_encoded_data'}"

    log("Input Validation", "passed _validate_input", log_type="debug")



def parse_input_data(raw_input):

    data = decode(raw_input)
    if type(data) == str:
        data = json.loads(data)

    return data

def main(request):
    try:
        TAG = "main - request parsing"
        log(TAG, "Beginning to parse request")
        log("Request", request, log_type="debug")
        request_json = request.get_json()
        log("JSON", request_json, log_type="debug")
        log(TAG, "Validating Input...")
        try:
            _validate_input(request_json)
        except AssertionError as ae:
            log("Error", str(ae))
            log("Error", str(traceback.print_exc()), log_type="debug")
            return json.dumps({"result": {'error': "Invalid input: " + str(ae)}})
        log(TAG, "Parsing tasks")
        tasks = parse_input_data(request_json["tasks"])
        log("Tasks", tasks, log_type="debug")
        log(TAG, "Parsing events")
        data_calender = parse_input_data(request_json["events"])
        log("Events", data_calender, log_type="debug")
        problem_name = parce_args("tomorrow") #TODO change on the bases of each plan
        log("Problem Name", str(problem_name), log_type="debug")
        tasks = get_tasks(tasks)
        log("Tasks (after get tasks)", str(tasks), log_type="debug")
        A_star_problem, state, cal, t = get_problem(problem_name, tasks, print_base=True, data=data_calender)
        start = time.time()
        max_value, solution = A_star_problem.calc_best_schedule(A_star_problem.base_day, tasks, 0)
        end = time.time()
        log(TAG, "Time for finding solution: " + str(end - start))
        log(TAG, "Number expanded by trim search: " + str(A_star_problem.expanded))
        response_data = {"events": []}
        log(TAG, "Best Solution Grade: {}".format(max_value))

        for event in solution:
            log("Solution Event", repr(event), log_type="debug")
            if event.moveable:
                response_data["events"].append(event.toDict())
        response = json.dumps({"result": response_data})
        log("Solution", solution, log_type="debug")
        return response
    except Exception as e:
        log("Error", str(e))
        log("Error", str(traceback.print_exc()), log_type="debug")
        return json.dumps({"result": {'error': "Some unknown error occurred. " + str(e)}})

def get_coded_events():
    with open("mock_data/new_encoded_events", 'r') as f:
        return f.read()
class test_request:

    req = {
        "events": get_coded_events() ,
        "tasks": 'eJx1jk0KwjAUhK8ib2tTmjQxSa8iLl5+hCKtUtuV9A6Kioh7qSd617FdiaC7Yb6Bb5YHCF2DbbmtoQAOCVTYbGKAom26mECNVRwBHVM6pSNtyyruW6x201rpjHOVSz2CrgxT5aS2wjhmvBJMSu2YQ+1ZiD64TEQbtIE++ZKKv9IbnWmY0YOu9BrDfEYD3elJl99HVGY+R6xAKa1jaLViMleG2UXQbI3Ke+eVsRyhX70BjOhTPQ=='
    }
    response = {
        "result":
            {
                "events": [
                    {'summary': 'example',
                     'start': '2019-09-28T08:00:00+00:00',
                     'end': '2019-09-28T09:00:00+00:00'
                     }
                ]
            }

    }

    def get_json(self):
        return self.req



class bad_request:
    req = {"tasks": [{"name": "1", "duration": 2}], "events": "kjdfjkfdfdss"}

    def get_json(self):
        return self.req


if __name__ == "__main__":
    # Input parameters (time interval)
    for TEST in (test_request(),):
        res = main(TEST)
        print(len(res))
        print("Result: " + str(res))



