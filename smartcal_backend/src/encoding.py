import json
import zlib  # compression
import base64  # base64 byte encoding

def encode(data):
    json_str = json.dumps(data)
    json_bytes = json_str.encode('utf-8')
    compressed = zlib.compress(json_bytes)

    b64 = base64.b64encode(compressed)
    as_string = b64.decode('utf-8')
    return as_string

    # return {'base64(zip(o))': as_string}

def decode(data, encoding=None):
    if type(data) == dict:
        data = data["base64(zip(o))"]
    as_bytes = data.encode('utf-8')
    decoded = base64.b64decode(as_bytes)
    decompress = zlib.decompress(decoded)
    json_str = decompress.decode('utf-8')
    return json.loads(json_str)
