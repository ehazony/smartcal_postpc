"""
defines search problem and algorithms for finding best assignment
"""

import math
from datetime import *
from src.calender_types import *

from src import search_util
from src.config import LOG_INFO, LOG_DEBUG
from src.search_structures import Node


class CalNode(Node):
    """
    node state for search algorithms
    """

    def __init__(self, base_day, events_data):
        super().__init__(state=base_day)
        self.events_to_schedule = events_data




class SearchProblem:
    """
    This class outlines the structure of a search_problem problem


    """

    def get_start_state(self):
        """
        Returns the start state for the search_problem problem
        """
        search_util.raiseNotDefined()

    def is_goal_state(self, state):
        """
        state: Search statex

        Returns True if and only if the state is a valid goal state
        """
        search_util.raiseNotDefined()

    def get_successors(self, state):
        """
        state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        search_util.raiseNotDefined()

    def get_cost_of_actions(self, actions):
        """
        actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        search_util.raiseNotDefined()

def log(tag="TAG", message="NO MESSAGE", log_type="info"):
    if log_type == "info":
        if LOG_INFO:
            print("{}: {}".format(tag, message))
    if log_type == "debug":
        if LOG_DEBUG:
            print("{}: {}".format(tag, message))

class ScheduleSearchProblem(SearchProblem):
    expanded = 0
    best_cost = math.inf
    def __init__(self, base_day: EventsList, cal: Calendar, start, end, events_data =None):
        """
        :param base_day: EventState holding all _events that are already in the time period, will
        :param events_data: tuple (summary, duration)
        :param cal: calendar
        """
        self.base_start_time = base_day._start
        self.base_end_time = base_day._end
        self.base_day = base_day
        self.events_to_schedule = events_data
        self.cal = cal
        self.extractor = CalendarInfoExtractor(cal)
        self._start = start
        self._end = end
        for event in self.base_day:  # make shure that _events are not moved in the search_problem
            event.set_moveable(False)
            log("ScheduleSearchProblem: ", event.start_time, log_type="debug")
            log("ScheduleSearchProblem: ", self._start, log_type="debug")
            assert event.start_time >= self._start
            assert event.end_time <= self._end

    def is_goal_state(self, node: CalNode):
        return len(node.events_to_schedule) == 0

    def get_start_state(self):
        return CalNode(self.base_day, self.events_to_schedule)



    def get_successors(self, node: CalNode):
        """
        :param node:
        :return: all options to assign EventData to base state
        """
        successors = []
        state = node.state
        free_intervals = state.get_free_intervals(self._start, self._end)
        for event_data in node.events_to_schedule:
            event_summary, event_delta = event_data.summary, event_data.duration
            placeable_intervals = []
            [placeable_intervals.extend(self.extractor.brake_interval(interval, event_delta)) for
             interval in free_intervals]
            for inter in placeable_intervals:
                events = copy.copy(state.get_events())
                events.append(Event(event_summary, inter.start, inter.end))
                assignmet = CalNode(EventsList(events, state.get_start(), state.get_end()),
                                    [d for d in node.events_to_schedule if d != event_data])
                successors.append(assignmet)
        return successors



    def get_cost_of_actions(self, actions):
        return 0

    def get_best_assignment_bfs(self, base_day, events_data, print_counter=False):
        """
        will calculate the best assignment of events_to_schedule by checking all passable states
        using day_heuristic to evaluate
        """
        if len(events_data) == 0:
            self.expanded += 1
            if print_counter:
                print(self.expanded)
            return day_value_heuristic(base_day, self.extractor), base_day
        free_intervals = base_day.get_free_intervals(self._start, self._end)
        event_summary, event_delta = events_data[0].summary, events_data[0].duration
        placeable_intervals = []
        [placeable_intervals.extend(self.extractor.brake_interval(interval, event_delta)) for
         interval in free_intervals]
        max_value = 0
        max_assignment = None
        for inter in placeable_intervals:
            events = copy.copy(base_day.get_events())
            events.append(Event(event_summary, inter.start, inter.end))
            assignmet = EventsList(events, base_day.get_start(), base_day.get_end())
            value, s = self.get_best_assignment_bfs(assignmet, events_data[1:], print_counter)
            # print("value: "+ str(value))
            if value > max_value:
                max_value = value
                max_assignment = s
        return max_value, max_assignment

    def calc_best_schedule(self, base_day, events_data, cur_plan_cost, print_counter=False):
        """
        will calculate the best assignment of events_to_schedule by checking all possible states
        using day_heuristic to evaluate
        """
        if len(events_data) == 0:
            if cur_plan_cost < self.best_cost:
                self.best_cost = cur_plan_cost
            return cur_plan_cost, base_day
        free_intervals = base_day.get_free_intervals(self._start, self._end)
        event_summary, event_delta = events_data[0].summary, events_data[0].duration
        placeable_intervals = []
        [placeable_intervals.extend(self.extractor.brake_interval(interval, event_delta)) for
         interval in free_intervals]
        max_value = math.inf
        max_assignment = None
        for inter in placeable_intervals:
            self.expanded += 1
            events = copy.copy(base_day.get_events())
            event = Event(event_summary, inter.start, inter.end)
            events.append(event)
            assignmet = EventsList(events, base_day.get_start(), base_day.get_end())
            new_cost = cur_plan_cost + day_min_value_hueristic(EventsList([event]), self.extractor)
            if new_cost > self.best_cost:
                value = math.inf
                s = math.inf
            else:
                value, s = self.calc_best_schedule(assignmet, events_data[1:], new_cost, print_counter)
            # print("value: "+ str(value))
            if value < max_value:
                max_value = value
                max_assignment = s
        return max_value, max_assignment

def null_heuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0



def day_value_heuristic(day: EventsList, extractor: CalendarInfoExtractor, print_scores=False):
    """:returns some of the intersecting_percentage of the _events in the day """
    events = day.get_events()
    some = 0
    one_offes = 0
    hit_some = 0
    for event in events:
        if event.moveable is True:
            value = extractor.intersecting_percentage(event) * extractor.hits_count(event)
            if print_scores:
                print(event.summary + ":   value=" + str(value))
            some += value
    return some

def day_min_value_hueristic(day: EventsList, extractor: CalendarInfoExtractor):
    """
    terns day heuristic to min function (smaller is better) if day heuristic is changes need to change this function to
    """
    # func =  lambda x: extractor.count_occurrences(x.summary) - extractor.intersecting_percentage(x) * extractor.hits_count(x)
    some = 0
    for event in day.get_events():
        if event.moveable is True:
            value = 1 - extractor.intersecting_percentage(event)
            some += value
    return some

if __name__ == "__main__":
    # input_data = []
    # # input_data.append(("חדר כושר", timedelta(hours=1)))
    # # input_data.append(("השקמה", timedelta(hours=1)))
    # # input_data.append(("ארוחת צהריים", timedelta(hours=1)))
    # input_data.append(EventData("חישוביות וחשיבה", timedelta(hours=2)))
    # input_data.append(EventData("קניות", timedelta(hours=3)))
    # # input_data.append(("לימוד לביולוגיה", timedelta(hours=2)))
    # cal = GoogleCalendar()
    # state = cal.get_today_events()
    # state.set_start(hour=8)
    # state.set_end(hour=20)
    #
    # problem = ScheduleSearchProblem(state, input_data, cal)
    # # print(problem.get_successors(CalNode(state, input_data)))
    # problem.get_Astar_assignment(CalNode(problem.base_day, problem.events_to_schedule), )
    # # a= a_star_search(problem, day_heuristic)

    # ---------------------------------------------

    data = []
    # input_data.append(("חדר כושר", timedelta(hours=1)))
    # input_data.append(("השקמה", timedelta(hours=1)))
    # input_data.append(("ארוחת צהריים", timedelta(hours=1)))
    data.append(("חישוביות וחשיבה", timedelta(hours=2)))
    data.append(("קניות", timedelta(hours=3)))
    # input_data.append(("לימוד לביולוגיה", timedelta(hours=2)))

    cal = MyAndroidSideCalendar()
    state = cal.get_today_events()
    state.set_start(hour=8)
    state.set_end(hour=22)
    problem = ScheduleSearchProblem(state, None, cal)
    # print(len(problem.extractor.occurrences_in_calendar()))
    # a = [i for i in problem.extractor.occurrences_in_calendar() if "שיחה" not in i]
    # print(problem.extractor.count_occurrences("חישוביות וחשיבה"))
    # for e in a:
    #     print(e)
    # state._events = []
    salution = problem.calc_best_schedule(state, data, 0, True)
    print("salution score: " + str(salution[0]))
    for event in salution[1]:
        if event.moveable:
            # cal.insert_event(event)
            print(event)
