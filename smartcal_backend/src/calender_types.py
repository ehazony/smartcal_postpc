"""
Defines calender objects i.e. event, calendar, calendar_extracter and EventsList.
"""

import datetime
import pickle
from abc import ABC, abstractclassmethod
import random
from pytz import reference, timezone
import copy
from src.datetime_interval import Interval
from tzlocal import get_localzone
from enum import Enum
import heapq
import json
import pytz

from src.config import DEBUG

# from Google_Cal_api import Google_Cal_api


local_tnz = reference.LocalTimezone()

SUMMERY = 0
DURATION = 1

DEFALT_TIME_ZONE = datetime.timezone.utc

class ProblemName(Enum):
    TODAY = 1
    TOMORROW = 2
    THIS_WEEK = 3
    NEXT_WEEK = 4
    ALL = 5


def parce(event_description):
    data = str.split(",")
    return data[SUMMERY], datetime.timedelta(minutes=int(data[DURATION]))

def isotime_to_datetime(time):
    """translates  RFC 3339 format to datetimeobject"""
    return datetime.datetime.strptime(''.join(time.rsplit(':', 1)), '%Y-%m-%dT%H:%M:%S%z')


class androidCalenderAPI:

    def __init__(self, data=None):
        if DEBUG or data == None:
            with open("mock_data/example_events.json", 'r') as f:
                self.events = json.load(f)
        else:
            self.events = data


    def _is_within_interval(self, event, interval : Interval):
        #TODO: implement

        g = GoogleEvent(event)
        if  (not g.summary() or not g.start_time() or not g.end_time()):
            return False
        if (g.get_interval() - interval):
            return True
        return False

    def get_events(self, start_time, end_time):
        assert start_time < end_time
        interval = Interval(start_time, end_time)
        ret_events = { #TODO change this to self.events format
            "kind": "calendar#events",
            "etag": "\"p33s9lacbrn7e80g\"",
            "summary": "efraimhazony@gmail.com",
            "updated": "2019-09-13T21:20:05.787Z",
            "timeZone": "Asia/Jerusalem",
            "accessRole": "owner",
            "defaultReminders": [],
            "nextPageToken": "EiUKGjc3czB0M2VwdjdpaTEwNW1ianFqa3U1MmduGICI14-96tUC",
            "items": []
        }
        for event in self.events["items"]:
            if self._is_within_interval(event, interval):
                ret_events["items"].append(event)

        return ret_events

    def insert_event(self, new_event):
        raise NotImplemented

    def get_all(self):
        return self.events


class EventData(ABC):
    def __init__(self, summary, duration):
        """
        :param summary: string
        :param duration: timeDelta
        """
        self.summary = summary
        self.duration = duration

    def __repr__(self):
        return "Event Data Object(Summary: {}, Duration: {})".format(self.summary, self.duration)


class Event(ABC):
    def __init__(self, summary, start_time, end_time, moveable=True):
        self.moveable = moveable
        self.start_time = start_time.replace(tzinfo=DEFALT_TIME_ZONE)
        self.end_time = end_time.replace(tzinfo=DEFALT_TIME_ZONE)
        self.summary = summary
        if self.start_time is not None and self.end_time is not None:
            self.interval = Interval(self.start_time, self.end_time)
        else:
            self.interval = None

    def __eq__(self, other):
        return self.start_time == other.start_time and self.end_time == other.end_time

    def __ne__(self, other):
        return not self.__eq__(other)

    def __lt__(self, other):
        if self.start_time < other.start_time:
            return True
        if self.start_time == other.start_time and self.end_time < other.end_time:
            return True
        return False

    def toDict(self):
        return {"summary": self.summary, "start": self.start_time.isoformat(), "end": self.end_time.isoformat()}

    def __repr__(self):
        return str(self.toDict())

    def __str__(self):
        return 'Event summary: %s, _start time %s, _end time %s' % (self.summary, self.start_time, self.end_time)

    @classmethod
    def create_random_event(self, data, start, end):  # todo test
        """
        :param summary for event
        :param duration of event
        :param start: floor for _start datetime object
        :param end: sealing for _end datetime object
        :return: randem event that is in the time range, _start time is in qurders of the hour
        """
        duration = data.duration
        summary = data.summary
        assert (duration >= datetime.timedelta(minutes=15))
        assert ((duration.total_seconds() % 3600) // 60 % 15 == 0)
        assert (end - start >= duration)
        delta = end - start
        seconds = (delta - duration).total_seconds()
        minutes, seconds = divmod(seconds, 60)
        i = int(minutes / 15)
        date_list = [start + datetime.timedelta(minutes=15 * x) for x in range(0, i + 1)]
        start_time = random.choice(date_list)
        end_time = start_time + duration
        assert (start <= start_time and start_time <= end_time and end_time <= end)
        return Event(summary, start_time, end_time)

    def summary(self):
        return self.summary

    def start_time(self):
        return self.start_time

    def end_time(self):
        return self.end_time

    def moveable(self):
        return self.moveable

    def set_start_time(self, start_time):
        self.start_time = start_time
        assert self.end_time >= self.start_time

    def set_end_time(self, end_time):
        self.end_time = end_time
        assert self.end_time >= self.start_time

    def set_moveable(self, bul):
        self.moveable = bul


class GoogleEvent(Event):
    """"Event object with google event as input_data"""

    def __init__(self, google_event_data, movable=False):
        self.event_data = google_event_data
        super().__init__(self.summary(), self.start_time(), self.end_time(), movable)

    def summary(self):
        try:
            return self.event_data['summary']
        except:
            return None

    def start_time(self):
        try:
            start_mills = self.event_data['start']['dateTime']['value']
            start_tz =  DEFALT_TIME_ZONE
            # start_tz =  self.event_data['start'].get('timeZone',DEFALT_TIME_ZONE)
            start = datetime.datetime.fromtimestamp( start_mills/ 1000.0)
            if(start_tz):
                start = start.replace(tzinfo= start_tz)
            else:
                start = start.replace(tzinfo=datetime.timezone.utc)
            return start
        except:
            return None

    def end_time(self):
        try:
            end_mills = self.event_data['end']['dateTime']['value']
            end_tz =  DEFALT_TIME_ZONE
            # end_tz =  self.event_data['end'].get('timeZone', DEFALT_TIME_ZONE)
            end = datetime.datetime.fromtimestamp( end_mills/ 1000.0)
            if(end_tz):
                end = end.replace(tzinfo= end_tz)
            else:
                end= end.replace(tzinfo=datetime.timezone.utc)
            return end

        except:
            return None

    def location(self):
        try:
            return self.event_data['location']
        except:
            return ""
    def get_interval(self):
        return Interval(self.start_time, self.end_time)


class Calendar(ABC):
    @abstractclassmethod
    def get_tomorrow_events(self):
        pass

    @abstractclassmethod
    def get_today_events(self):
        pass

    @abstractclassmethod
    def get_week_events(self):
        pass

    @abstractclassmethod
    def get_nextWeek_events(self):
        pass

    @abstractclassmethod
    def get_all_events(self):
        pass

    def get_events(self, when: ProblemName):
        if ProblemName.TODAY == when:
            return self.get_today_events()
        if ProblemName.TOMORROW == when:
            return self.get_tomorrow_events()
        if ProblemName.THIS_WEEK == when:
            return self.get_week_events()
        if ProblemName.NEXT_WEEK == when:
            return self.get_nextWeek_events()
        if ProblemName.ALL == when:
            return self.get_all_events()


class EventsList:
    """an object for holding a group of _events"""

    def __init__(self, events: [Event], start=None, end=None):
        """
        :param events: a list of _events to be stored ( with start, end, summary) not none
        :param start: datetime object, default is 6 am today
        :param end: datetime object, default is 18 pm today
        """
        self._start = start
        self._end = end
        self._events = []
        self.timezone = DEFALT_TIME_ZONE
        if start is None and end is None:  # set start and end to today
            self._start = datetime.datetime.now(self.timezone)
            self._start = self._start.replace(hour=6, minute=0, second=0, microsecond=0)
            # self.start.tzinfo = local_tnz.utcoffset(self.start)
            self._end = datetime.datetime.now(self.timezone)
            self._end = self._end.replace(hour=18, minute=0, second=0, microsecond=0)

            # if _events in self._events not in default time (start,end) fix start and end time
            for event in events:
                event.start_time = event.start_time.replace(tzinfo=self.timezone)
                event.end_time = event.end_time.replace(tzinfo=self.timezone)
                if event.start_time < self._start:
                    self._start = event.start_time
                if event.end_time > self._end:
                    self._end = event.end_time

        elif start is None or end is None:
            raise Exception

        # self.end.tzinfo = local_tnz.utcoffset(self.end)
        # self.timezone = local_tnz.utcoffset(self._start)
        self.set_start(tzinfo=self.timezone)
        self.add_events_list(events)


    def __iter__(self):
        return iter(self._events)

    def __repr__(self):
        return self._events.__repr__()

    def __str__(self):
        return self._events.__str__()

    def get_events(self):
        return self._events

    def set_start(self, year=None, month=None, day=None, hour=None, minute=None,
                  second=None, microsecond=None, tzinfo=None):
        if year is not None:
            self._start = self._start.replace(year=year)
        if month is not None:
            self._start = self._start.replace(month=month)
        if day is not None:
            self._start = self._start.replace(day=day)
        if hour is not None:
            self._start = self._start.replace(hour=hour)
        if minute is not None:
            self._start = self._start.replace(minute=minute)
        if minute is not None:
            self._start = self._start.replace(second=second)
        if minute is not None:
            self._start = self._start.replace(microsecond=microsecond)
        if tzinfo is not None:
            self._start = self._start.replace(tzinfo=tzinfo)
        e = []
        for event in self._events:
            if event.start_time >= self._start:
                e.append(event)
            elif event.end_time > self._start:
                event.start_time = copy.copy(self._start)
                e.append(event)
        self._events = e

    def set_end(self, year=None, month=None, day=None, hour=None, minute=None,
                second=None, microsecond=None, tzinfo=None):

        if year is not None:
            self._end = self._end.replace(year=year)
        if month is not None:
            self._end = self._end.replace(month=month)
        if day is not None:
            self._end = self._end.replace(day=day)
        if hour is not None:
            self._end = self._end.replace(hour=hour)
        if minute is not None:
            self._end = self._end.replace(minute=minute)
        if minute is not None:
            self._end = self._end.replace(second=second)
        if minute is not None:
            self._end = self._end.replace(microsecond=microsecond)
        if tzinfo is not None:
            self._end = self._end.replace(tzinfo=tzinfo)
        e = []
        for event in self._events:
            if event.end_time <= self._end:
                heapq.heappush(e, event)
            elif event.start_time < self._end:
                event.end_time = copy.copy(self._end)
                heapq.heappush(e, event)
        self._events = e

    def get_start(self):
        return self._start

    def get_end(self):
        return self._end

    def add_event(self, event):
        assert event.start_time is not None
        assert event.end_time is not None
        assert event.summary is not None

        # assert event.start_time.tzinfo == event.end_time.tzinfo == self.timezone
        # TODO not shoure we need start and end in eventList
        # if event.start_time < self._start or event.end_time > self._end:
        #     raise Exception
        heapq.heappush(self._events,event)


    def add_events_list(self, events):
        for event in events:
            self.add_event(event)

    def add_random_event(self, data, start=None, end=None, moveable=True):  # todo test
        """
        creats randem event and adds it to its _events
        :param summary for event
        :param timedelta  with the duration of event
        :param start: datetime object representing floor for _start
        :param end: datetime object representing seling for _end
        :param moveable can the event be moved by the app
        """
        if not start:
            start = self._start
        if not end:
            end = self._end
        assert start <= end
        assert end - start >= data.duration
        #todo change add event
        self.add_event(Event.create_random_event(data.summary, data.duration, start, end).set_moveable(moveable))

    def get_free_intervals(self, start, end):
        """
        :returns  a list of Intervels, were each interval represents a time in the  Eventstate that dose not have
        a event scheduled on it.
        :return: [Interval]
        """
        # TODO will want this to guest return free_intervals witch will be calculated in the constructor
        if len(self._events) == 0:
            return [Interval(start, end)]

        self._events.sort()
        intervals = []
        if start < self._events[0].start_time:
            intervals.append(Interval(start, self._events[0].start_time))
        for i in range(len(self._events) - 1):
            if self._events[i].end_time < self._events[i + 1].start_time:
                intervals.append(Interval(self._events[i].end_time, self._events[i + 1].start_time))
        if self._end > self._events[-1].end_time:
            intervals.append(Interval(self._events[-1].end_time, end))
        return intervals

    def get_shiffted_up_eventsStates(self, duration=datetime.timedelta(minutes=15)):
        """
        :returns a list of EventStates that each one has a shifted up event in it
        """
        # Todo deal with adjacent _events
        # TODO not completed  function
        sheffted_events = []
        free_intervals = self.get_free_intervals()
        for i in range(len(self._events)):
            events = copy.deepcopy(self._events)
            for interval in free_intervals:
                if events[i].end_time in interval and events[i].end_time + duration in interval:
                    events[i].start_time = events[i].start_time + duration
                    events[i].end_time = events[i].end_time + duration
                    sheffted_events.append(EventsList(events, self._start, self._end))
                    break
        return sheffted_events

    def get_shiffted_down_eventsStates(self, duration=datetime.timedelta(minutes=15)):
        """
        :returns a list of eventsSattes that each one hase a shiffted down event in it
        """
        # Todo deal with adjacent _events
        # TODO not completed  function
        sheffted_events = []
        free_intervals = self.get_free_intervals()
        for i in range(len(self._events)):
            events = copy.deepcopy(self._events)
            for interval in free_intervals:
                if events[i].start_time in interval and events[i].start_time - duration in interval:
                    events[i].start_time = events[i].start_time - duration
                    events[i].end_time = events[i].end_time - duration
                    sheffted_events.append(EventsList(events, self._start, self._end))
                    break
        return sheffted_events

    def schedule_random_events(self, events_data, moveable=True):
        """
        ads new _events in random times in to the EventsList
        :param events_data: [(summary, duration)]      summary is string, duration is timedelta
        """
        for event_data in events_data:
            free_intervals_copy = self.get_free_intervals()
            schedule = False
            while len(free_intervals_copy) > 0:
                i = random.choice(range(len(free_intervals_copy)))
                if event_data[1] > free_intervals_copy[i].duration:
                    free_intervals_copy.pop(i)
                else:
                    self.add_random_event(event_data[0], event_data[1], free_intervals_copy[i].start,
                                          free_intervals_copy[i].end, moveable)
                    schedule = True
                    break
            if not schedule:  # no interval found to put the event
                return None
        return True


class MyAndroidSideCalendar(Calendar):
    # TODO update calender metheds to return  events from db
    def __init__(self, data=None):
        self.android_cal_api = androidCalenderAPI(data)
        # self.service = self.google_cal_api.service
        self.timezone = timezone('Asia/Jerusalem')
        self.google_events =self.dict_to_GoogleEvents(self.android_cal_api.get_all())

    def get_tomorrow_events(self):
        today = datetime.datetime.now(self.timezone).replace(hour=6, minute=0, second=0, tzinfo=DEFALT_TIME_ZONE)
        tomorrow = today + datetime.timedelta(days=1)
        end = tomorrow.replace(hour=22)
        i = Interval(tomorrow, end)
        return self.get_events_in_interval(i)

    def get_today_events(self):
        today = datetime.datetime.now(self.timezone).replace(hour=6, minute=0, second=0, tzinfo=DEFALT_TIME_ZONE)
        end = today.replace(hour=22)
        i = Interval(today, end)
        return self.get_events_in_interval(i)


    def get_week_events(self):
        today = datetime.datetime.now(self.timezone).replace(hour=8, minute=0, second=0, microsecond=0)
        idx = (today.weekday() + 1) % 7
        sun = today - datetime.timedelta(idx)
        saturday = sun + datetime.timedelta(days=7)
        return self.get_events_in_interval(Interval(sun, saturday))

    def get_nextWeek_events(self):
        today = datetime.datetime.now(self.timezone).replace(hour=0, minute=0, second=0)
        idx = (today.weekday() + 1) % 7
        sun = today - datetime.timedelta(idx - 7)
        saturday = sun + datetime.timedelta(days=7)
        return self.get_events_in_interval(Interval(sun, saturday))

    def get_all_events(self, months=12):
        """get all _events """

        return EventsList(self.google_events)

        # return EventsList(self.dict_to_GoogleEvents(

    def dict_to_GoogleEvents(self, events):
        """
        :returns all legal events in items
        """
        g_events = []
        for event in events.get('items', []):
            e = GoogleEvent(event)
            if(e.start_time and e.end_time and e.summary):
                g_events.append(e)
        return g_events

    def _event_to_json(self, event: Event):
        """event format to be added to google calender"""
        # timeZone = timezone("UTC")
        # timeZone.utcoffset(2)
        # event.start_time=event.start_time.replace(tzinfo=timeZone)
        # event.end_time=event.end_time.replace(tzinfo=timeZone)
        # TODO this is a paster to work only in are timezone needs to be changed!
        start = event.start_time + datetime.timedelta(minutes=21)
        end = event.end_time + datetime.timedelta(minutes=21)
        EVENT = {
            'summary': event.summary,
            'start': {'dateTime': start.isoformat()},
            'end': {'dateTime': end.isoformat()},
        }
        return EVENT

    def insert_event(self, event: GoogleEvent):
        self.android_cal_api.insert_event(self._event_to_json(event))

    def insert_events(self, events: [GoogleEvent]):
        for event in events:
            self.insert_event(event)

    def get_events_in_interval(self, i):
        temp_events = []
        for e in self.google_events:
            if(e.interval - i ):
                temp_events.append(e)
        return EventsList(temp_events, i.start, i.end)


class CalendarInfoExtractor:
    def __init__(self, cal: Calendar):
        self.cal = cal
        #todo: cannot write files on cloud
        try:
            # pass
            self.occurrences_intervals = pickle.load(open("occurrences_intervals.pickle", "rb"))
        except (OSError, IOError) as e:
            self.events = cal.get_all_events()
            self.occurrences_intervals = self.get_accurences_intervals(self.events)
            # pickle.dump(self.occurrences_intervals, open("occurrences_intervals.pickle", "wb"))

        # for x in range(1000):
        #     print(self.intersecting_percentage(self._events._events[x]))

    def occurrences_in_calendar(self, min_occurences=3):
        summarys = []
        for summary, intervlas in self.occurrences_intervals.items():
            if len(intervlas) >= min_occurences:
                summarys.append(summary)
        return summarys

    def count_occurrences(self, summary):
        """
        :returns how many occurrences of the event is in occurrences_intervals
        """
        if summary in self.occurrences_intervals.keys():
            return len(self.occurrences_intervals[summary])
        return 0

    def intersecting_percentage(self, event: Event):
        """
        :param event: event with _start and _end time
        :return: will compute the percentage of overlap between older _events with the same name as the event
        i.e. average (time overlap/ duration of event)
        """
        summary = event.summary.strip()
        assert event is not None
        assert summary is not None
        if summary not in self.occurrences_intervals:
            return 0  # TODO should this be the value for event that we havent seen?

        total_time_of_event = datetime.timedelta(0)
        intersection_sum = datetime.timedelta(0)

        for interval in self.occurrences_intervals[summary]:
            total_time_of_event += interval.to_timedelta()
            intersection_sum += interval.daytime_intersection_delta(event.interval)

        # print("------------------------")
        # print(event.summary)
        # print(total_time_of_event)
        # print(intersection_sum)
        # print("------------------------")
        return (intersection_sum / total_time_of_event)

    def hits_count(self, event: Event):
        """
        computes the number of times the event hit events
        :return:
        """
        hits_counter = 0
        summary = event.summary.strip()
        if summary not in self.occurrences_intervals:
            return 0  # TODO should this be the value for event that we havent seen?
        for interval in self.occurrences_intervals[summary]:
            if interval.daytime_intersection_delta(event.interval) > datetime.timedelta(0):
                hits_counter += 1
        return hits_counter


    def get_accurences_intervals(self, events):
        """
        :returns dict{summery, [intervals]} were intervals are the intervals in witch the event has
        accrued
        """
        d = {}
        for event in events:
            if event is None or event.summary is None:
                continue
            summary = event.summary.strip()
            if summary not in d and event.interval is not None:
                d[summary] = []
            if event.interval is not None:  # has a beginning and _end
                d[summary].append(event.interval)
        return d

    # def intervel_intersection_delta(self, interval: Interval, interval1: Interval):
    #     """
    #     :returns timedelta with the duration of intersection of the time
    #      in the day of the to intervals
    #     """
    #     interval._start.timetuple()
    #     return
    def brake_interval(self, interval: Interval, delta: datetime.timedelta):
        """
        splits the Interval into delta sized Intervals (15 m differince)
        """
        intervals = []
        fifteen_delta = datetime.timedelta(minutes=15)
        # deltas were being passed as integers too often, causing bugs.
        if type(delta) in (int, float, str):
            delta = datetime.timedelta(hours=float(delta))
        # print("delta: {}\ninterval: {}".format(delta, interval.to_timedelta()))
        if delta > interval.to_timedelta() :
            return []
        i = interval.start
        while i <= interval.end - delta:
            intervals.append(Interval(i, delta))
            i = i + fifteen_delta
        return intervals


def full_events(events):
    """
    :returns only _events that have legal parameters start_time end_time and summary
    """
    # TODO make as static methed
    if events is None: return None
    full = []
    for event in events:
        if not (event.start_time is None or event.end_time is None or event.summary is None):
            full.append(event)
    return full


# if __name__ == "__main__":
#     cal = MyAndroidSideCalendar()
#     events = cal.get_today_events()
#     # [print(i) for i in events.get_free_intervals()]
#     [print(e) for e in events]
# print("---------------------")
# for eventss in _events.get_shiffted_up_eventsStates():
#     [print(e) for e in eventss]
#     print("----------------------")
#
# [print(e) for e in _events]
