package data;

import android.graphics.Color;
import android.util.Log;

import com.alamkanak.weekview.WeekViewEvent;


import com.example.postpc_smartCal.R;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import java.util.Calendar;

public class GoogleWeekViewEvent extends WeekViewEvent {

    private int baseColor = Color.parseColor("#abe5ff");


    public GoogleWeekViewEvent(Event event){
        if (
                event == null
                || event.getStart() == null
                || event.getEnd() == null
                || event.getStart().getDateTime() == null
                || event.getEnd().getDateTime() == null
        ){
            return;
        }
        Calendar startTime = Calendar.getInstance();
        startTime.setTimeInMillis(event.getStart().getDateTime().getValue());
        Calendar endTime = (Calendar) startTime.clone();
        endTime.setTimeInMillis(event.getEnd().getDateTime().getValue());
        this.setId(1);
        this.setName(event.getSummary());
        this.setLocation(event.getLocation());
        this.setStartTime(startTime);
        this.setEndTime(endTime);
        this.setToBaseColor();

    }

    public GoogleWeekViewEvent(ResponseEvent event){
        DateTime start = new DateTime(event.getStart());
        DateTime end = new DateTime(event.getEnd());
        Log.i("GetPlanWork", "GoogleWeekViewEvent: Start as DateTime --- " + start.toString());
        Calendar startTime = Calendar.getInstance();
        startTime.setTimeInMillis(start.getValue());
        Log.i("GetPlanWork", "GoogleWeekViewEvent: Start as Calendar --- " + startTime.toString());

        Calendar endTime = (Calendar) startTime.clone();
        endTime.setTimeInMillis(end.getValue());
        this.setId(1);
        this.setName(event.getSummary());
        this.setLocation(null);
        this.setStartTime(startTime);
        this.setEndTime(endTime);
        this.setColor(Color.parseColor("#29b6f6")); //  secondary
        Log.i("GetPlanWork", "GoogleWeekViewEvent: Start as GoogleWorkViewEvent" + this.getStartTime().toString());

    }

    public void setToBaseColor() {
        Log.i("Event Color", "setToBaseColor: "  + this.toString());
        this.setColor(baseColor); // primary
    }

    public Event toGoogleEvent() {
        Event googleEvent = new Event()
                .setSummary(this.getName())
                .setLocation(this.getLocation());
        DateTime startDateTime = new DateTime(this.getStartTime().getTime());
        EventDateTime start = new EventDateTime()
                .setDateTime(startDateTime)
                .setTimeZone(this.getStartTime().getTimeZone().getID());
        googleEvent.setStart(start);

        DateTime endDateTime = new DateTime(this.getEndTime().getTime());
        EventDateTime end = new EventDateTime()
                .setDateTime(endDateTime)
                .setTimeZone(this.getEndTime().getTimeZone().getID());
        googleEvent.setEnd(end);

        return googleEvent;
    }
}
