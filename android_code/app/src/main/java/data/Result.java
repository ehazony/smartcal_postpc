package data;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("events")
    @Expose
    private List<ResponseEvent> events = null;

    public List<ResponseEvent> getEvents() {
        return events;
    }

    public void setEvents(List<ResponseEvent> responseEvents) {
        this.events = responseEvents;
    }

}