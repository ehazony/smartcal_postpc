
package work;
import android.content.Context;
import android.util.Log;

import com.example.postpc_smartCal.AppDataManager;
import com.example.postpc_smartCal.utils;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import data.GoogleWeekViewEvent;
import data.ResponseEvent;
import data.SetPlanTasksRequest;
import data.TasksPlanResponse;
import retrofit2.Response;
import server.MyOfficeServerInterface;
import server.ServerHolder;

/**
 * worker will incopulate the 2 strings and send them to the lambda function to find plan
 */
public class GetPlanWork extends Worker {
    AppDataManager manger;
    String ENCODING = "base64(zip(o))";
    public GetPlanWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        manger = AppDataManager.getInstance(context);
    }

    @NonNull
    @Override
    public Result doWork() {
        Gson g = new Gson();
        Log.i("FLOW", "doWork: in GetPlanWork");

        MyOfficeServerInterface serverInterface = ServerHolder.getInstance().serverInterface;
        String tasks_data  =getInputData().getString("tasks");
        String events_data = new Gson().toJson(manger.getCalender());
//        String tasks_data = new Gson().toJson(tasks);
        String encoded_events_data = utils.encodeString(events_data);
        String encoded_tasks_data = utils.encodeString(tasks_data);
        SetPlanTasksRequest dataRequest = new SetPlanTasksRequest(encoded_tasks_data,encoded_events_data);
        Log.i("GSON", "doWork: "+g.toJson(dataRequest));

        try {
            Response<TasksPlanResponse> response = serverInterface.getPlan(dataRequest).execute();
            TasksPlanResponse res = response.body();
            Log.i("FLOW", "doWork response body: " + res.toString());
            String tokenJson = new Gson().toJson(res);
            Data outputData = new Data.Builder()
                    .putString("output", tokenJson)
                    .build();
            updateManger(res);
            return Result.success(outputData);
        } catch (IOException e) {
            e.printStackTrace();
            return Result.retry();
        }
    }

    private void updateManger(TasksPlanResponse response){
        List<GoogleWeekViewEvent> eventsList = new ArrayList<>();
            for(ResponseEvent event : response.getResult().getEvents()){
                GoogleWeekViewEvent googleEvent = new GoogleWeekViewEvent(event);
                eventsList.add(googleEvent);
                Log.i("GetPlanWork: ","r name: "  + event.getSummary());
                Log.i("GetPlanWork: ","r start: " + event.getStart());
                Log.i("GetPlanWork: ","r end:"    + event.getEnd());
                Log.i("GetPlanWork: ","g name: "  + googleEvent.getName());
                Log.i("GetPlanWork: ","g start: " + googleEvent.getStartTime());
                Log.i("GetPlanWork: ","g end:"    + googleEvent.getEndTime());

        }

        manger.updateLoadedWeekViewEvents(eventsList);
    }
}
