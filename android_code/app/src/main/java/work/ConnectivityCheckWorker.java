//package work;
//
//import com.example.ex8_postpc.server.MyOfficeServerInterface;
//import com.example.ex8_postpc.server.ServerHolder;
//
//import java.io.IOException;
//import androidx.annotation.NonNull;
//import androidx.work.Worker;
//import androidx.work.WorkerParameters;
//import retrofit2.Response;
//
//public class ConnectivityCheckWorker extends Worker {
//    public ConnectivityCheckWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
//        super(context, workerParams);
//    }
//
//    @NonNull
//    @Override
//    public Result doWork() {
//        MyOfficeServerInterface serverInterface = ServerHolder.getInstance().serverInterface;
//
//        try {
//            Response<TokenResponse> step1response = serverInterface.connectivityCheck().execute();
//            return Result.success();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            return Result.retry();
//        }
//    }
//}