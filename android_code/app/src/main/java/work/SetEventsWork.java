package work;

import android.accounts.Account;
import android.content.Context;

import com.example.postpc_smartCal.AppDataManager;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;

import java.io.IOException;
import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import data.GoogleWeekViewEvent;

/**
 *
 */
public class SetEventsWork extends Worker {
    Context thiscontext;
    AppDataManager appDataManager;

    public SetEventsWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        thiscontext = context;
        appDataManager = AppDataManager.getInstance(thiscontext);
    }

    @NonNull
    @Override
    public Result doWork() {
        appDataManager = AppDataManager.getInstance(thiscontext);
        Account account = appDataManager.getGoogleSignInAccount().getAccount();
        final String APPLICATION_NAME = "Google Calendar API Java Quickstart";
        final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
        GoogleAccountCredential credential =
                GoogleAccountCredential.usingOAuth2(thiscontext,
                        Collections.singleton("https://www.googleapis.com/auth/calendar.events"));
        credential.setSelectedAccount(account);
        final NetHttpTransport HTTP_TRANSPORT = new com.google.api.client.http.javanet.NetHttpTransport();
        Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
        try {
            for(GoogleWeekViewEvent event : appDataManager.loadedWeekViewEvents.getValue()) {
                service.events().insert("primary", event.toGoogleEvent() ).execute();
                event.setToBaseColor();
                appDataManager.addEventToCalender(event);

        }
                return Result.success();
        } catch (IOException e) {
            e.printStackTrace();
            return Result.retry();
        }
    }
}
