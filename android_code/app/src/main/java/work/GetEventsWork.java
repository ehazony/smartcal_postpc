package work;

import android.accounts.Account;
import android.content.Context;
import android.util.Log;

import com.example.postpc_smartCal.AppDataManager;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.google.gson.Gson;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

/**
 * worker will incopulate the 2 strings and send them to the lambda function to find plan
 */
public class GetEventsWork extends Worker {
    Context thiscontext;
    AppDataManager manger;

    public GetEventsWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        thiscontext = context;

    }

    @NonNull
    @Override
    public Result doWork() {
        manger = AppDataManager.getInstance(thiscontext);
        String account_json = getInputData().getString("account");
        Account account = new Gson().fromJson(account_json, Account.class);
        final String APPLICATION_NAME = "Google Calendar API Java Quickstart";
        final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
        GoogleAccountCredential credential =
                GoogleAccountCredential.usingOAuth2(thiscontext,
                        Collections.singleton("https://www.googleapis.com/auth/calendar.events"));
        credential.setSelectedAccount(account);
        final NetHttpTransport HTTP_TRANSPORT = new com.google.api.client.http.javanet.NetHttpTransport();
        Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
        DateTime now = new DateTime(System.currentTimeMillis());
        Events events;
        ArrayList<Events> allEvents = new ArrayList<>();
        try {
            String pageToken = null;
            ArrayList<Event> allLegalItems = new ArrayList();
            do {
                events = service.events().list("primary").setPageToken(pageToken).execute();

                List<Event> items = events.getItems();

                for (Event event : items) {
                    if (event == null ||event.getStart()==null|| event.getEnd()==null|| event.getStart().getDateTime() == null || event.getEnd().getDateTime() == null){
                        continue;
                    }
                    allLegalItems.add(event);
                }
                allEvents.add(events);
                pageToken = events.getNextPageToken();
            } while (pageToken != null);
            events = allEvents.get(0);
            events.setItems(allLegalItems);
            manger.setCalender(events);
            Log.i("FLOW", "doWork: got events from calender server");
            Log.i("FLOW", "number of events: "+  allLegalItems.size());

            manger.calenderLoaded.postValue(true);
//            Data outputData = new Data.Builder()
//                    .putString("output", tokenJson)
//                    .build();
//            manger.calenderLoaded.postValue(true);
            return Result.success();
        } catch (IOException e) {
            e.printStackTrace();
            return Result.retry();
        }
    }
}
