package com.example.postpc_smartCal;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


public class TaskRecyclerUtils {

    static class TaskCallback
            extends DiffUtil.ItemCallback<calenderTask> {

        @Override
        public boolean areItemsTheSame(@NonNull calenderTask calenderTask, @NonNull calenderTask t1) {
            return calenderTask.name.equals(t1.name);
        }


        @Override
        public boolean areContentsTheSame(@NonNull calenderTask calenderTask, @NonNull calenderTask t1) {
            return calenderTask.equals(t1);
        }
    }

    interface TaskClickCallback {
        void onTaskClick(calenderTask calenderTask);
    }

    //    interface TaskSwipeCallback{
//        void onTaskSwipe(calenderTask calenderTask);
//    }
    static class MessageAdapter
            extends ListAdapter<calenderTask, TaskHolder> {


        private int markedCounter;

        public MessageAdapter() {
            super(new TaskCallback());
        }

        public TaskClickCallback callback;

        @NonNull
        @Override
        public TaskHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            final Context context = viewGroup.getContext();
            View itemView = LayoutInflater.from(context).inflate(R.layout.item_one_task, viewGroup, false);
            final TaskHolder holder = new TaskHolder(itemView);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calenderTask calenderTask = getItem(holder.getAdapterPosition());
                    if (callback != null) {
                        callback.onTaskClick(calenderTask);
                        if(!calenderTask.marked){
                            holder.activate_task_button_on.setVisibility(View.GONE);
                            holder.activate_task_button_off.setVisibility(View.VISIBLE);
                        }
                        else{
                            holder.activate_task_button_on.setVisibility(View.VISIBLE);
                            holder.activate_task_button_off.setVisibility(View.GONE);
                        }
                    }

                }

            });

            return holder;


        }

        @Override
        public void onBindViewHolder(@NonNull TaskHolder messageHolder, int i) {
            calenderTask calenderTask = getItem(i);
            messageHolder.textView.setText(calenderTask.name);
            messageHolder.durationView.setText(calenderTask.duration);
            if (calenderTask.marked) {
                messageHolder.activate_task_button_on.setVisibility(View.VISIBLE);
                messageHolder.activate_task_button_off.setVisibility(View.GONE);

            } else {
                messageHolder.activate_task_button_on.setVisibility(View.GONE);
                messageHolder.activate_task_button_off.setVisibility(View.VISIBLE);
            }
        }


    }


    private static class TaskHolder
            extends RecyclerView.ViewHolder {
        public final TextView textView;
        public final TextView durationView;
        public final ImageButton delete_task_button;
        public final ImageButton edit_task_button;
        public final ImageButton activate_task_button_off;
        public final ImageButton activate_task_button_on;

        public TaskHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.task_name);
            durationView = itemView.findViewById(R.id.task_duration);
            delete_task_button = itemView.findViewById(R.id.delete_task_button);
            edit_task_button = itemView.findViewById(R.id.edit_task_button);
            activate_task_button_off = itemView.findViewById(R.id.activate_task_button_off);
            activate_task_button_on = itemView.findViewById(R.id.activate_task_button_on);
        }
    }


}
