package com.example.postpc_smartCal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;
//import com.google.api.services.calendar.Calendar;
//import com.google.api.Backend;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.FirebaseApp;
import com.google.gson.Gson;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import data.TasksPlanResponse;
import work.GetPlanWork;
import work.GetEventsWork;
import work.SetEventsWork;


/**
 * Code for the calenderTasks list activity, witch is the fist activity that is show on the screen after log in
 */
public class MainActivity extends AppCompatActivity implements TaskRecyclerUtils.TaskClickCallback {
    public static final String HAS_NAME = "HAS_NAME";
    private static final String CALENDER_NOT_LOADED = "calender has not loaded yet...";
    public static final String SIGNOUT = "SIGNOUT";
    private static final String UPDATED_CALENDER = "google calender updated";
    public static final String FROM_CALENDAR_VIEW_NAV = "FROM_CALENDAR_VIEW_NAV";
    public static String USER_NAME = "USER_NAME";
    static final String TEXT_VIEW_HOLDER = "boxText";

    private static final String MESSAGES_HOLDER = "messages_holder";
    private static final String EMPTY_TASK_MESSAGE = "Task name and duration are required";
    public static final String GOOGLE_ACCOUNT = "google_account";
    public static final String ALL_EVENTS = "google_account";

    private AppDataManager manger;
    private static TaskRecyclerUtils.MessageAdapter adapter = new TaskRecyclerUtils.MessageAdapter();
    private static AppRoomDatabase db;
    //    private AppDataManager db;
    private ArrayList<calenderTask> calenderTasks;

    private Button planScheduleButton = null;
    private RecyclerView recyclerView = null;

    // Below edittext and button are all exist in the popup dialog view.
    private View popupInputDialogView = null;
    // Contains user name data.
    private EditText summeryEditText = null;
    // Contains password data.
    private EditText durationEditText = null;
    // Contains email data.
    private EditText otherInputEditText = null;
    // Click this button in popup dialog to save user input data in above three edittext.
    private Button saveTaskDataButton = null;
    // Click this button to cancel edit user data.
    private Button cancelSaveTaskDataButton = null;
    private NumberPicker numberPickerHour;
    private NumberPicker numberPickerMinute;
    GoogleSignInAccount googleSignInAccount;
    private int markedCounter = 0;
    private String FINISHED_LOADING_CALENDER = "Calender loaded and is up to date ";
    private String FALED_TO_LOAD_CALENDER = "Failed to calculate schedule, try again...";
    private ImageView menuButton = null;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Tasks");
        Spannable text = new SpannableString("Create Tasks");
        text.setSpan(new ForegroundColorSpan(Color.parseColor("#4f5b62")), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(text);
        googleSignInAccount = getIntent().getParcelableExtra(GOOGLE_ACCOUNT);
        FirebaseApp.initializeApp(this);
        try {
            readCalendarEventsWork();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        this.manger = AppDataManager.getInstance(this);
        manger.setGoogleSignInAccount(googleSignInAccount);
        recyclerView = findViewById(R.id.Recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        // setting swipe to Planning activity
        initPlanScheduleButton();
        planScheduleButton.setVisibility(View.GONE);


        initMenu();
        db = AppRoomDatabase.getAppDatabase(this);
        calenderTasks = new ArrayList<>(db.TaskDao().getAll());
        initMarked();

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {


            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                final int position = viewHolder.getAdapterPosition(); //get position which is swipe
                if (swipeDir == ItemTouchHelper.RIGHT) {    //if swipe left
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this); //alert for confirm to delete
                    builder.setMessage("Are you sure to delete?");    //set message
                    builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() { //when click on DELETE
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //item removed from recylcerview
                            adapter.notifyItemRemoved(position);
//                            sqldatabase.execSQL("delete from " + TABLE_NAME + " where _id='" + (position + 1) + "'"); //query for delete
                            deleteTask(calenderTasks.get(position));//then remove item
                            return;
                        }
                    }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            adapter.notifyItemRemoved(position + 1);    //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                            adapter.notifyItemRangeChanged(position, adapter.getItemCount());   //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                            return;
                        }
                    });
                    Dialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setCancelable(false);
                    dialog.show();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        Log.e("onCreate", "number of calenderTasks uploaded: " + calenderTasks.size());

        adapter.callback = this;
        initFloatingButton();
        final Observer<ArrayList<calenderTask>> nameObserver = new Observer<ArrayList<calenderTask>>() {
            @Override
            public void onChanged(ArrayList<calenderTask> messages1) {
                calenderTasks = new ArrayList<>(messages1);
                adapter.submitList(calenderTasks);
            }

        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        manger.tasksLiveData.observe(this, nameObserver);

        //        final Observer<Boolean> eventsLoadedObserver = new Observer<Boolean>() {
//            @Override
//            public void onChanged(Boolean aBoolean) {
//                if (aBoolean) {
//                    // done loading calender
//                    Log.i("readEvents", "onChanged: got into info");
//                    getPlanForTasksWork();
//                }
//            }
//        };
//        manger.calenderLoaded.observe(this, eventsLoadedObserver);
//        adapter.submitList( calenderTasks);
    }

    private void initUpdateCalenderObserver() {

    }


    private void initMenu() {
        menuButton = findViewById(R.id.expand_menu);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOutAccount();
            }
        });

    }

    private void initMarked() {
        markedCounter = 0;
        for (calenderTask task : calenderTasks) {
            if (task.marked) {
                markedCounter++;
            }
        }
        if (markedCounter == 0) {
            planScheduleButton.setVisibility(View.GONE);
        } else {
            planScheduleButton.setVisibility(View.VISIBLE);
        }
    }

    private void initPlanScheduleButton() {
        planScheduleButton = findViewById(R.id.gotoPlanning);
        planScheduleButton.setVisibility(View.GONE);
        planScheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (manger.calenderLoaded.getValue()) {
                    getPlanForTasksWork();
                    Intent intent = new Intent(MainActivity.this, PlanningActivity.class);
                    intent.putExtra(FROM_CALENDAR_VIEW_NAV, false);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), CALENDER_NOT_LOADED, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void initFloatingButton() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create a AlertDialog Builder.
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                // Set title, icon, can not cancel properties.
                alertDialogBuilder.setTitle("Add Task");
                alertDialogBuilder.setIcon(R.drawable.ic_launcher_background);
                alertDialogBuilder.setCancelable(false);
                // Init popup dialog view and it's ui controls.
                initPopupViewControls();
                // Set the inflated layout view object to the AlertDialog builder.
                alertDialogBuilder.setView(popupInputDialogView);
                // Create AlertDialog and show.
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                // When user click the save user data button in the popup dialog.
                saveTaskDataButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Get user data from popup dialog editeext.
                        String summery = summeryEditText.getText().toString();
                        String duration = String.valueOf(numberPickerHour.getValue());
                        String other = otherInputEditText.getText().toString();

                        if (summery.equals("") || duration.equals("")) {
                            Toast.makeText(getApplicationContext(), EMPTY_TASK_MESSAGE, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        calenderTask m = new calenderTask(summery, duration);
                        ArrayList<calenderTask> copyCalenderTasks = new ArrayList<>(calenderTasks);
                        copyCalenderTasks.add(m);
                        calenderTasks = copyCalenderTasks;
                        adapter.submitList(calenderTasks);
                        manger.addToDb(m);
                        alertDialog.cancel();
                    }
                });

                cancelSaveTaskDataButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.cancel();
                    }
                });
            }
        });
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
//        final EditText editText = (EditText) findViewById(R.id.editText);
//        savedInstanceState.putString(TEXT_VIEW_HOLDER, String.valueOf(editText.getText()));
        super.onSaveInstanceState(savedInstanceState);
        //saves to firebase on closing of the app
        manger.tasksLiveData.setValue(calenderTasks);

// To restart user name:
//        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
//        editor.putBoolean("HAS_NAME", false);
//        editor.putString(USER_NAME, "test");
//        editor.commit();
    }

    //
    @Override
    public void onTaskClick(final calenderTask calenderTask) {
        if (calenderTask.marked) {
            calenderTask.marked = false;
            markedCounter--;
            if (markedCounter == 0) {
                planScheduleButton.setVisibility(View.GONE);
            }

        } else {
            calenderTask.marked = true;
            markedCounter++;
            planScheduleButton.setVisibility(View.VISIBLE);
        }
    }


    /* Initialize popup dialog view and ui controls in the popup dialog. */
    private void initPopupViewControls() {
        // Get layout inflater object.
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);

        // Inflate the popup dialog from a layout xml file.
        popupInputDialogView = layoutInflater.inflate(R.layout.popup_input_dialog, null);

        // Populate dropdown choices



        Spinner staticSpinner = (Spinner) popupInputDialogView.findViewById(R.id.static_spinner);
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.time_intervals,
                        android.R.layout.simple_spinner_item);
        // ... Specify the layout to use when the list of choices appears
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // ... Apply the adapter to the spinner
        staticSpinner.setAdapter(staticAdapter);
        staticSpinner.setSelection(1); // Set to Hour by default

        // Do something with the item selected from spinner
        staticSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Default is first
                Log.v("item", (String) parent.getItemAtPosition(0));
            }
        });
        // Get user input edittext and button ui controls in the popup dialog.
        summeryEditText = (EditText) popupInputDialogView.findViewById(R.id.task_name);
        durationEditText = (EditText) popupInputDialogView.findViewById(R.id.duration_input);
        otherInputEditText = (EditText) popupInputDialogView.findViewById(R.id.comment);
        saveTaskDataButton = popupInputDialogView.findViewById(R.id.button_add_task);
        cancelSaveTaskDataButton = popupInputDialogView.findViewById(R.id.button_cancel_task);
        numberPickerHour = (NumberPicker) popupInputDialogView.findViewById(R.id.hr_timepicker);
        numberPickerHour.setMinValue(0);
        numberPickerHour.setMaxValue(10);
        numberPickerHour.setWrapSelectorWheel(true);
        numberPickerMinute =  (NumberPicker) popupInputDialogView.findViewById(R.id.min_timepicker);
        String[] valuse = {"0", "15", "30", "45"};
        numberPickerMinute.setDisplayedValues(valuse);
        numberPickerMinute.setMinValue(0);
        numberPickerMinute.setMaxValue(valuse.length-1);
        numberPickerMinute.setWrapSelectorWheel(true);
    }


    private void getPlanForTasksWork() {
        Gson gson = new Gson();
        Log.i("FLOW", "getPlanForTasksWork: got into getPlanForTasksWork");
        List<calenderTask> markedTasks = new ArrayList<>();
        for (calenderTask task : calenderTasks) {
            if (task.marked) {
                markedTasks.add(task);
            }
        }
        String tasks_json = gson.toJson(markedTasks);
        UUID workTagUniqueId = UUID.randomUUID();
        OneTimeWorkRequest getInfoW = new OneTimeWorkRequest.Builder(GetPlanWork.class)
                .setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build())
                .setInputData(new Data.Builder().putString("tasks", tasks_json).build())
                .addTag(workTagUniqueId.toString())
                .build();
        WorkManager.getInstance().enqueue(getInfoW);
        WorkManager.getInstance().getWorkInfosByTagLiveData(workTagUniqueId.toString()).observe(this, new Observer<List<WorkInfo>>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onChanged(List<WorkInfo> workInfos) {
                if (workInfos == null || workInfos.isEmpty() || workInfos.get(0).getOutputData().size() == 0) {
//                    Toast.makeText(getApplicationContext(), FALED_TO_LOAD_CALENDER, Toast.LENGTH_SHORT).show();
                    return;
                }
                WorkInfo info = workInfos.get(0);
                String ticketAsJson = info.getOutputData().getString("output");
                TasksPlanResponse userInfoRespose = new Gson().fromJson(ticketAsJson, TasksPlanResponse.class);

            }
        });


    }

    public void readCalendarEventsWork() throws IOException, GeneralSecurityException {
        Gson gson = new Gson();
        String account_json = gson.toJson(googleSignInAccount.getAccount());
        UUID workTagUniqueId = UUID.randomUUID();
        OneTimeWorkRequest getInfoW = new OneTimeWorkRequest.Builder(GetEventsWork.class)
                .setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build())
                .setInputData(new Data.Builder().putString("account", account_json).build())
                .addTag(workTagUniqueId.toString())
                .build();

        WorkManager.getInstance().enqueue(getInfoW);
        WorkManager.getInstance().getWorkInfosByTagLiveData(workTagUniqueId.toString()).observe(this, new Observer<List<WorkInfo>>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onChanged(List<WorkInfo> workInfo) {
                if (workInfo == null || workInfo.isEmpty()) {
                    return;
                }
                WorkInfo work = workInfo.get(0);
                boolean finished = work.getState().isFinished();
                if (finished) {
                    Toast.makeText(getApplicationContext(), FINISHED_LOADING_CALENDER, Toast.LENGTH_LONG).show();
                }
//                String ticketAsJson = info.getOutputData().getString("output");
//                allEventsHolder = new Gson().fromJson(ticketAsJson, EventsHolder.class);
//         set manger so that data will be reachable from calender
//                manger.setCalender(allEventsHolder);
            }
        });
    }


    public void deleteTask(calenderTask calendertask) {
        calendertask.marked = false;
        initMarked();
        ArrayList<calenderTask> tasksCopy = new ArrayList<>(calenderTasks);
        tasksCopy.remove(calendertask);
        calenderTasks = tasksCopy;
        adapter.submitList(calenderTasks);
        manger.tasksLiveData.setValue(calenderTasks);
        manger.deleteFromDB(calendertask);

    }

    public void signOutAccount() {
        Intent myIntent = new Intent(this, SignInActivity.class);
        myIntent.putExtra(MainActivity.SIGNOUT, true);
        myIntent.putExtra(MainActivity.GOOGLE_ACCOUNT, googleSignInAccount);
        MainActivity.this.finish();
        startActivity(myIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.expand_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout_nav:
                Log.i("Menu Item", "onOptionsItemSelected: " + "logout");
                signOutAccount();
                // do something
                break;

            case R.id.about_nav:
                Log.i("Menu Item", "onOptionsItemSelected: " + "about");
                break;

            case R.id.help_nav:
                Log.i("Menu Item", "onOptionsItemSelected: " + "help");
                break;

            case R.id.calendar_view_nav:
                Log.i("Menu Item", "onOptionsItemSelected: " + "calendar");
                Intent intent = new Intent(MainActivity.this, PlanningActivity.class);
                intent.putExtra(FROM_CALENDAR_VIEW_NAV, true);
                startActivity(intent);
                break;

            default:
                return super.onContextItemSelected(item);

        }
        return true;
    }

}









