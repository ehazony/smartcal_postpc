package com.example.postpc_smartCal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity
public
class calenderTask implements Parcelable {


    @NonNull
    @PrimaryKey()
    public String uid;

    @ColumnInfo(name= "name")
    public String name;

    @ColumnInfo(name= "duration")
    public String duration;

    @ColumnInfo(name = "timestamp")
    private String timestamp;


    @Ignore
    calenderTask(){}
    @Ignore
    public Boolean marked;

        calenderTask(String name, String duration) {
        this.name = name;
        Long tsLong = System.currentTimeMillis()/1000;
        this.timestamp = tsLong.toString();
        this.uid = UUID.randomUUID().toString();
        this.duration = duration;
        this.marked = false;
    }

    public calenderTask(Parcel in) {
        this.name = in.readString();
        this.timestamp= in.readString();
        this.uid = in.readString();
        this.duration = in.readString();
        this.marked = false;
    }

//    public static calenderTask makeMessage(String data){
//        return new calenderTask("null",data);
//    }
    public static ArrayList<calenderTask> getAll() {
        ArrayList<calenderTask>a = new ArrayList<calenderTask>();
//        a.add(new calenderTask("df","test string"));
        return a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        calenderTask task = (calenderTask) o;
        return uid.equals(task.uid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(this.timestamp);
        dest.writeString(this.uid);
        dest.writeString(this.duration);

    }
    public static final Parcelable.Creator<calenderTask> CREATOR = new Parcelable.Creator<calenderTask>() {
        public calenderTask createFromParcel(Parcel in) {
            return new calenderTask(in);
        }

        public calenderTask[] newArray(int size) {
            return new calenderTask[size];
        }
    };

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


}
