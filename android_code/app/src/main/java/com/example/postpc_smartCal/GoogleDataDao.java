package com.example.postpc_smartCal;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


@Dao
public interface GoogleDataDao {
    @Query("SELECT * FROM GoogleEvents")
    List<GoogleEvents> getAll();

    @Insert
    void insertAll(GoogleEvents... GoogleEvents);

    @Delete
    void delete(GoogleEvents googleEvents);

    @Query("SELECT * FROM GoogleEvents WHERE uid = :id LIMIT 1")
    List<GoogleEvents> getItemId(String id);
}


