package com.example.postpc_smartCal;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.alamkanak.weekview.WeekViewEvent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.api.services.calendar.model.Events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import data.GoogleWeekViewEvent;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AppDataManager {

    private static AppDataManager Manger;
    private final FirebaseFirestore firestore;
    private CollectionReference mDatabaseRef;
    private AppRoomDatabase localDatabase;
    public MutableLiveData<ArrayList<calenderTask>> tasksLiveData = new MutableLiveData<>();
    private Executor executor = Executors.newCachedThreadPool();
    public AppRoomDatabase roomDb;
    public String userName;
    private GoogleSignInAccount googleSignInAccount;
    private Events Calender;
    public MutableLiveData<Boolean> calenderLoaded = new MutableLiveData<>();
    public MutableLiveData<List<GoogleWeekViewEvent>> loadedWeekViewEvents = new MutableLiveData<List<GoogleWeekViewEvent>>();
    public MutableLiveData<Boolean> updateCalender ;



//    List messages;


    private AppDataManager(Context context) {
        calenderLoaded.postValue(false);
        updateCalender = new MutableLiveData<>();
        updateCalender.postValue(false);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        userName = sp.getString(MainActivity.USER_NAME, null);
        roomDb = AppRoomDatabase.getAppDatabase(context);
        tasksLiveData.setValue(new ArrayList<calenderTask>());
        firestore = FirebaseFirestore.getInstance();
        firestore.collection("defaults").document(userName).
                collection("texts").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
                        //updates messages from firebase to lockal db
                        if (task.isSuccessful() && task.getResult() != null) {
                            ArrayList<calenderTask> allItemsFromFirestore = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                allItemsFromFirestore.add(document.toObject(calenderTask.class));
                            }
//                                    AppDataManager.this.tasksLiveData.postValue(allItemsFromFirestore);

                            for (calenderTask m : roomDb.TaskDao().getAll()) {
                                if (!allItemsFromFirestore.contains(m)) {
                                    roomDb.TaskDao().delete(m);
                                }
                            }
                            for (calenderTask m : allItemsFromFirestore) {
                                if (roomDb.TaskDao().getItemId((String.valueOf(m.uid))).size() == 0) {
                                    roomDb.TaskDao().insertAll(m);
                                }
                            }

                            tasksLiveData.setValue(new ArrayList<calenderTask>(roomDb.TaskDao().getAll()));
                        }
                    }
                });
    }

    public static AppDataManager getInstance(Context context) {
        if (Manger == null) {
            Manger = new AppDataManager(context);
        }
        return Manger;
    }


    public void addToDb(final calenderTask m) {
        executor.execute(
                new Runnable() {
                    @Override
                    public void run() {
                        firestore.collection("defaults").document(userName).
                                collection("texts").add(m);
                        roomDb.TaskDao().insertAll(m);
                    }
                });
    }

    public void deleteFromDB(final calenderTask m) {
        executor.execute(
                new Runnable() {
                    @Override
                    public void run() {
                        firestore.collection("defaults").document(userName).
                                collection("texts").whereEqualTo("uid", m.uid).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        firestore.collection("defaults").document(userName).
                                                collection("texts").document(document.getId())
                                                .delete()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w(TAG, "Error deleting document", e);
                                                    }
                                                });
                                    }
                                } else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                            }
                        });
                        roomDb.TaskDao().delete(m);
                        tasksLiveData.postValue(new ArrayList<calenderTask>(roomDb.TaskDao().getAll()));
                    }
                });
    }

    public void addUserName(final String name) {
        this.userName = name;
        executor.execute(
                new Runnable() {
                    @Override
                    public void run() {
//                        firestore.collection("defaults").add(name, {//
//                        });
                        Map<String, Object> data = new HashMap<>();

                        firestore.collection("defaults").document(name)
                                .set(data, SetOptions.merge());

                        for (calenderTask m : roomDb.TaskDao().getAll()) {
                            firestore.collection("defaults").document(userName).
                                    collection("texts").add(m);
                        }
                    }
                });
    }

    public MutableLiveData getLiveData() {
        return this.tasksLiveData;
    }



    public Events getCalender() {
        return Calender;
    }

    public void setCalender(Events calender) {
        // TODO change calender to WeekViewEvents
//        List<WeekViewEvent>
        this.Calender = calender;
    }

    public void updateLoadedWeekViewEvents(List<GoogleWeekViewEvent> eventsList) {
        this.loadedWeekViewEvents.postValue(eventsList);
    }

    public GoogleSignInAccount getGoogleSignInAccount() {
        return googleSignInAccount;
    }

    public void setGoogleSignInAccount(GoogleSignInAccount googleSignInAccount) {
        this.googleSignInAccount = googleSignInAccount;
    }

    public void updateCalender() {
        this.updateCalender.postValue(true);
    }

    public void addEventToCalender(GoogleWeekViewEvent event) {
        this.Calender.getItems().add(event.toGoogleEvent());
    }


    //
//    public void updateRemoteData() {
//        firestore.collection("texts").get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
//                        executor.execute(new Runnable() {
//                            @Override
//                            public void run() {
//                                List<calenderTask> messages = tasksLiveData.getValue();
//                                if (task.isSuccessful() && task.getResult() != null) {
//                                    ArrayList<calenderTask> allItemsFromFirestore = new ArrayList<>();
//                                    for (QueryDocumentSnapshot document : task.getResult()) {
//                                        calenderTask m = document.toObject(calenderTask.class);
//                                        // removing message that was deleted in this session
//                                        if( !messages.contains(m)){
//                                            firestore.collection("texts").document(document.getId())
//                                                    .delete()
//                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
//                                                        @Override
//                                                        public void onSuccess(Void aVoid) {
//                                                            Log.d(TAG, "DocumentSnapshot successfully deleted!");
//                                                        }
//                                                    })
//                                                    .addOnFailureListener(new OnFailureListener() {
//                                                        @Override
//                                                        public void onFailure(@NonNull Exception e) {
//                                                            Log.w(TAG, "Error deleting document", e);
//                                                        }
//                                                    });
//                                        }
//                                        else{
//                                            allItemsFromFirestore.add(m);
//                                        }
//                                    }
//                                    // Adding messages that were added in this session
//                                    for (calenderTask m: Objects.requireNonNull(messages)){
//                                        if(!allItemsFromFirestore.contains(m)){
//                                            firestore.collection("texts").add(m);
//                                            Log.d(TAG, "adding message to firebase with id: " + m.uid + "--> " + m.data);
//                                        }
//                                    }
//                                }
//                            }
//                        });
//                    }
//                });
////    }
//    public void saveToDBCalender(List<Events> eventsList){
//        this.Calender = eventsList;
////        for(Events events: eventsList){
////            roomDb.GoogleDataDao().insertAll(new GoogleEvents(new Gson().toJson(events)));
////        }
//    }
//    public List<Events>  getDBCalender(){
////        List<GoogleEvents> eventsList = roomDb.GoogleDataDao().getAll();
////        List<String> newEventsList= new ArrayList<>();
////        for(GoogleEvents events: eventsList){
////            newEventsList.add(events.getData());
////        }
////        return newEventsList;
//        return Calender;
//    }

}
