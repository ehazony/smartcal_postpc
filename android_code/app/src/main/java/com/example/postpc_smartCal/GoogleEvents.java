package com.example.postpc_smartCal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity
class GoogleEvents implements Parcelable {


    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name= "data")
    public String data;


    @Ignore
    GoogleEvents(){}

    GoogleEvents(String data) {
        this.data = data;
    }

    public GoogleEvents(Parcel in) {
        this.data =in.readString();
        this.uid = in.readInt();

    }


//    public static ArrayList<calenderTask> getAll() {
//        ArrayList<calenderTask>a = new ArrayList<calenderTask>();
////        a.add(new calenderTask("df","test string"));
//        return a;
//    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        calenderTask person = (calenderTask) o;
//        return data.equals(person.name);
//    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(data);
        dest.writeInt(this.uid);
    }

    public static final Parcelable.Creator<calenderTask> CREATOR = new Parcelable.Creator<calenderTask>() {
        public calenderTask createFromParcel(Parcel in) {
            return new calenderTask(in);
        }

        public calenderTask[] newArray(int size) {
            return new calenderTask[size];
        }
    };

    public String getData(){
        return data;
    }




}
