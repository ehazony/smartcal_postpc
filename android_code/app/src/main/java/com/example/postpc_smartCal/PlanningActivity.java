package com.example.postpc_smartCal;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.Observer;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import data.GoogleWeekViewEvent;
import work.SetEventsWork;

import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.google.gson.Gson;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * this activity will show the planning calender
 */
public class PlanningActivity extends CalenderActivity {
    private AppDataManager manger;
    private TextView mTextMessage;
    private Button accept_button;
    private Button reject_button;
    private static final String UPDATED_CALENDER = "google calender updated";

//    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
//            = new BottomNavigationView.OnNavigationItemSelectedListener() {
//
//        @Override
//        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//
//            switch (item.getItemId()) {
//                case R.id.navigation_home:
//                    //TODO this is an exmaple how we can use the Actions bar to set the calender view
//                    mTextMessage.setText(R.string.title_dashboard);
//                    WeekView mWeekView = (WeekView) findViewById(R.id.weekView);
//                    item.setChecked(!item.isChecked());
//                    mWeekView.setNumberOfVisibleDays(1);
//                    // Lets change some dimensions to best fit the view.
//                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
//                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
//                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
//                    return true;
//                case R.id.navigation_dashboard:
//                    mTextMessage.setText(R.string.title_dashboard);
//                    return true;
//                case R.id.navigation_notifications:
//                    mTextMessage.setText(R.string.title_notifications);
//                    return true;
//            }
//            return false;
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_planning);
        super.onCreate(savedInstanceState);
        setTitle("");
        manger = AppDataManager.getInstance(this);
        initPlanningButtons();
        ProgressBar spinner = (ProgressBar) findViewById(R.id.progressBar);
        Bundle extra = getIntent().getExtras();
        if (extra != null && extra.getBoolean(MainActivity.FROM_CALENDAR_VIEW_NAV, false)) {
            spinner.setVisibility(View.GONE);
            reject_button.setVisibility(View.GONE);
            accept_button.setVisibility(View.GONE);
        } else {
            spinner.setVisibility(View.VISIBLE);
            WeekView weekview = (WeekView) findViewById(R.id.weekView);
            weekview.setVisibility(View.GONE);
        }

        mTextMessage = (TextView) findViewById(R.id.message);
//        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
//        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // stop spinner when done loading calender
        final Observer<List<GoogleWeekViewEvent>> nameObserver = new Observer<List<GoogleWeekViewEvent>>() {
            @Override
            public void onChanged(List<GoogleWeekViewEvent> googleWeekViewEvents) {
                if(manger.loadedWeekViewEvents.getValue() != null) {
                    ProgressBar spinner = (ProgressBar) findViewById(R.id.progressBar);
                    WeekView weekview = (WeekView) findViewById(R.id.weekView);
                    // done loading calender
                    spinner.setVisibility(View.GONE);
                    weekview.setVisibility(View.VISIBLE);
                    accept_button.setVisibility(View.VISIBLE);
                    reject_button.setVisibility(View.VISIBLE);
                }
            }
        };
        manger.loadedWeekViewEvents.observe(this, nameObserver);
    }

    private void initPlanningButtons() {

        accept_button = findViewById(R.id.accept_button);
        reject_button = findViewById(R.id.reject_button);
        accept_button.setVisibility(View.GONE);
        reject_button.setVisibility(View.GONE);
        accept_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setEventsWork();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                }
            }
        });
        reject_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manger.loadedWeekViewEvents.setValue(null);
                if (manger.tasksLiveData.getValue().size() > 0) {
                    for (calenderTask task : manger.tasksLiveData.getValue()) {
                        task.marked = false;
                    }
                }
                Intent myIntent = new Intent(PlanningActivity.this, MainActivity.class);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        if (!manger.calenderLoaded.getValue()) {
            return new ArrayList<>();
        }
        Events eventsList = this.manger.getCalender();
        List<WeekViewEvent> allEvents = new ArrayList<>();
        List<Event> items = eventsList.getItems();
        for (Event item : items) {
            GoogleWeekViewEvent event = new GoogleWeekViewEvent(item);
            Calendar cal = Calendar.getInstance();
            cal.setTime(event.getStartTime().getTime());
            int month = cal.get(Calendar.MONTH);
            int year = cal.get(Calendar.YEAR);
            if (month == newMonth && year == newYear) {
                allEvents.add(event);
            }
        }

        if (manger.loadedWeekViewEvents.getValue() == null) {
            return allEvents;
        }
        // if not null add events from planning results
        for (GoogleWeekViewEvent event : manger.loadedWeekViewEvents.getValue()) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(event.getStartTime().getTime());
            int month = cal.get(Calendar.MONTH);
            int year = cal.get(Calendar.YEAR);
            if (month == newMonth && year == newYear) {
                allEvents.add(event);
            }
        }
        return allEvents;
    }

    //
    public void setEventsWork() throws IOException, GeneralSecurityException {
        Gson gson = new Gson();

        UUID workTagUniqueId = UUID.randomUUID();
        OneTimeWorkRequest getInfoW = new OneTimeWorkRequest.Builder(SetEventsWork.class)
                .setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build())
                .setInputData(new Data.Builder().build())
                .addTag(workTagUniqueId.toString())
                .build();

        WorkManager.getInstance().enqueue(getInfoW);
        WorkManager.getInstance().getWorkInfosByTagLiveData(workTagUniqueId.toString()).observe(this, new Observer<List<WorkInfo>>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onChanged(List<WorkInfo> workInfo) {
                if (workInfo == null || workInfo.isEmpty()) {
                    return;
                }
                WorkInfo work = workInfo.get(0);
                boolean finished = work.getState().isFinished();
                if (finished) {
                    Toast.makeText(getApplicationContext(), UPDATED_CALENDER, Toast.LENGTH_SHORT).show();
                    accept_button.setVisibility(View.GONE);
                    reject_button.setVisibility(View.GONE);
                    // delete tasks that where updated
                    ArrayList<calenderTask> tasksCopy = new ArrayList<>(manger.tasksLiveData.getValue());
                    for (calenderTask task : manger.tasksLiveData.getValue()) {
                        if (task.marked) {
                            tasksCopy.remove(task);
                            manger.deleteFromDB(task);
                        }
                    }
                    manger.tasksLiveData.setValue(tasksCopy);
                    // delete result from manger
                    manger.loadedWeekViewEvents.setValue(null);
                    // add new events to calender
                    Intent intent = getIntent();
                    finish();
                    intent.putExtra(MainActivity.FROM_CALENDAR_VIEW_NAV, true);
                    startActivity(intent);
                    //TODO update ui (delete tasks from list..)
                }

            }
        });
    }
}
