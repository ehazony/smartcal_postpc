package com.example.postpc_smartCal;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {calenderTask.class, GoogleEvents.class}, version =18 ,exportSchema = false)
public abstract class AppRoomDatabase extends RoomDatabase {
    private static AppRoomDatabase INSTACE ;

    public abstract TaskDao TaskDao();
    public abstract GoogleDataDao GoogleDataDao();


    public static AppRoomDatabase getAppDatabase(Context context){
        if(INSTACE == null){
            INSTACE = Room.databaseBuilder(context.getApplicationContext(), AppRoomDatabase.class, "user-database").fallbackToDestructiveMigration()
                    .allowMainThreadQueries().build();
        }
        return INSTACE;
    }


}
