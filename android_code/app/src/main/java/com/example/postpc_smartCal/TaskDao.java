package com.example.postpc_smartCal;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


@Dao
public interface TaskDao {
    @Query("SELECT * FROM calenderTask")
    List<calenderTask> getAll();

    @Insert
    void insertAll(calenderTask... calenderTasks);

    @Delete
    void delete(calenderTask calenderTask);

    @Query("SELECT * FROM calenderTask WHERE uid = :id LIMIT 1")
    List<calenderTask> getItemId(String id);
}


