package com.example.postpc_smartCal;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class utils {

    private static byte[] compressByteArray(byte[] bytes){

        ByteArrayOutputStream baos = null;
        Deflater dfl = new Deflater();
//        dfl.setLevel(Deflater.BEST_COMPRESSION);
        dfl.setInput(bytes);
        dfl.finish();
        baos = new ByteArrayOutputStream();
        byte[] tmp = new byte[4*1024];
        try{
            while(!dfl.finished()){
                int size = dfl.deflate(tmp);
                baos.write(tmp, 0, size);
            }
        } catch (Exception ex){
            Log.e("compression", "compressByteArray: ." + ex);

        } finally {
            try{
                if(baos != null) baos.close();
            } catch(Exception ex){
                Log.e("compression", "compressByteArray: ." + ex);
            }
        }

        return baos.toByteArray();
    }

    private static byte[] decompressByteArray(byte[] bytes)  {
        ByteArrayOutputStream baos = null;
        Inflater infl = new Inflater();
        infl.setInput(bytes);
        infl.finished();
        baos = new ByteArrayOutputStream();
        byte[] tmp = new byte[4*1024];

        try{
            while(!infl.finished()){
                int size = infl.inflate(tmp);
                baos.write(tmp, 0, size);
            }
        } catch (Exception ex){
            Log.e("compression", "compressByteArray: ." + ex);

        } finally {
            try{
                if(baos != null) baos.close();
            } catch(Exception ex){
                Log.e("compression", "compressByteArray: ." + ex);
            }
        }
        return baos.toByteArray();


    }

    private static byte[] trim(byte[] output)
    {
        int i = output.length - 1;
        while(output[i] == 0)
        {
            --i;
        }
        return Arrays.copyOf(output, i + 1);
    }

    public static String encode(JSONObject data) throws JSONException {
        String jsonString = data.toString(0);
        byte[] jsonBytes = jsonString.getBytes();

        // Compress the bytes
        byte[] compressed = compressByteArray(jsonBytes);
        byte[] trimmed = trim(compressed);

        // Decode the bytes into a String
        return Base64.getEncoder().encodeToString(trimmed);
    }

    public static String encodeString(String data) {
        byte[] jsonBytes = data.getBytes();

        // Compress the bytes
        byte[] compressed = compressByteArray(jsonBytes);
        byte[] trimmed = trim(compressed);

        // Decode the bytes into a String
        return Base64.getEncoder().encodeToString(trimmed);
    }

    public static JSONObject decode(String data) throws JSONException {
        byte[] decoded = Base64.getDecoder().decode(data);
        byte[] decompressed = decompressByteArray(decoded);
        byte[] trimmed = trim(decompressed);
        String jsonString = new String(trimmed);
        return new JSONObject(jsonString);
    }

    public static String decodeString(String data) {
        byte[] decoded = Base64.getDecoder().decode(data);
        byte[] decompressed = decompressByteArray(decoded);
        byte[] trimmed = trim(decompressed);
        return new String(trimmed);
    }
}