package server;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerHolder {

//    private static final String BASE_URL = "https://us-central1-smartcal-1565715510537.cloudfunctions.net/";
    private static final String BASE_URL = "https://us-central1-smartcal-249519.cloudfunctions.net/smartCal/";
    private static ServerHolder instance = null;
    public final MyOfficeServerInterface serverInterface;

    public synchronized static ServerHolder getInstance() {
        if (instance != null)
            return instance;

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(1, TimeUnit.MINUTES) // connect timeout
                .writeTimeout(1, TimeUnit.MINUTES) // write timeout
                .readTimeout(1, TimeUnit.MINUTES);
        OkHttpClient client = builder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL) // notice the absence of the last slash!
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MyOfficeServerInterface serverInterface = retrofit.create(MyOfficeServerInterface.class);
        instance = new ServerHolder(serverInterface);
        return instance;
    }


    public ServerHolder(MyOfficeServerInterface serverInterface) {
        this.serverInterface = serverInterface;
    }

//    public static void updateToken(){
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.getContext());
//        String token = sp.getString(MainActivity.TOKEN, "");
//        if(!token.equals("")){
//            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new MyOkHttpInterceptor())
//                    .build();
//
//            Retrofit retrofit = new Retrofit.Builder()
//                    .client(client)
//                    .baseUrl(BASE_URL) // notice the absence of the last slash!
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//
//            MyOfficeServerInterface serverInterface = retrofit.create(MyOfficeServerInterface.class);
//            instance = new ServerHolder(serverInterface);
//        }
//    }

}

